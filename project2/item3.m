function [C, C0] = item3(RA, RB)
    rpm = 1000;
    life = 5500; %hours
    L10 = rpm * 60 * life / 1000000 %[millions of revs]
    
    
    
    P = zeros(1,2);
    %Find max loads
    P(1) = max(abs(RA(:))) %The 1st element in P, C0, and C are for bearing A
    P(2) = max(abs(RB(:))) %The 2nd element in P, C0, and C are for bearing B
    
    
    %% Static Analysis
    fs = 2; %static safety factor
    C0 = fs.*P;
    
    
    %% Dynamic Analysis
    
    C = L10^(1/3).*P;
    
    disp(['Bearing A: Static Safety C0 ', num2str(C0(1)), 'N,   Dynamic Safety C ', num2str(C(1)), 'N']);
    disp(['Bearing B: Static Safety C0 ', num2str(C0(2)), 'N,   Dynamic Safety C ', num2str(C(2)), 'N']);

end