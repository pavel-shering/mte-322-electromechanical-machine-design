%% Linearization and Closed-loop poles

clear all;
clc;

mw = 20; % [kg] wheel mass
Iw = 0.1; % [kg*m^2] inertia of the wheel
mh = 60; % [kg] humna mass
Ih = 10; % [kg*m^2] inertia of the wheel

rw = 0.25; % wheel radius 
l = 1; % [m] center of mass of the human form the wheel axis
g = 9.81; % [kg/m^2]
bw = 0; %

K = [-70, -250, -1800, -600];

syms s theta_h X Xd Tw

Tw = (-K(1) -s*K(2))*X + (K(1)+s*K(2))*Xd - (K(3)+s*K(4))*theta_h;
ode1 = (s^2)*(mh*l)*X - (s^2*(Ih - mh*l^2) - mh*g*l)*theta_h  == Tw;
ode2 = s^2*(rw*(mw+mh)+Iw/rw)*X + s^2*mh*rw*l*theta_h == Tw;

sol = solve(ode1,theta_h)==solve(ode2,theta_h)
TF = simplifyFraction(solve(sol,X)/Xd)
poles(TF,s)