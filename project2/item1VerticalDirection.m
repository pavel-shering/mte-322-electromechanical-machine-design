function [V, M, EITheta, EIDeflection, Ay, By] = item1VerticalDirection(Fcf, str)
    
    % singularity functions
    step_sf = @(x,a) (x>a);
    lin_sf = @(x,a) (x-a).*(x>a);
    quad_sf = @(x,a) ((x-a).^2).*(x > a)./2;
    cubic_sf = @(x,a) ((x-a).^3).*(x > a)./6;
    
    m_clothes = 4.5;
    m_drum = 20;
   
    %distances
    a = 0.025;
    b = 0.090;
    c = 0.200;
    g = 9.81;
    
    %forces 
    Fp = 600;
    Fd = g * (m_clothes + m_drum);
    Fl = Fd - Fcf;
    
    %dividing the linspace for 316 to get mm distances on the shaft
    z = linspace(0,a+b+c+0.00001,316);

    %Moment about A to solve for By
    By = (a*Fp - (b+c)*Fl) / -b;

    %Summation of Forces in Y Direction
    Ay = Fp + Fl - By;

    syms C1 C2;
    
    % Shear and Moment Diagrams Via Singularity Functions
    V = -Fp*step_sf(z,0) + Ay*step_sf(z,a) + By*step_sf(z,(a+b)) - (Fl)*step_sf(z,(a+b+c));
    M = -Fp*lin_sf(z,0) + Ay*lin_sf(z,a) + By*lin_sf(z,(a+b)) - (Fl)*lin_sf(z,(a+b+c));
    %EITheta = -Fp*quad_sf(z,0) + Ay*quad_sf(z,a) + By*quad_sf(z,(a+b)) - (Fl)*quad_sf(z,(a+b+c))+C1;
    EIDeflection = -Fp*cubic_sf(z,0) + Ay*cubic_sf(z,a) + By*cubic_sf(z,(a+b)) - (Fl)*cubic_sf(z,(a+b+c)) + C1*z + C2;
    
    indexA = find(abs(z-a) < 0.0001);
    indexB = find(abs(z-a-b) < 0.0001);
    
    [sol1, sol2] = solve([0 == EIDeflection(1,indexA) + z(indexA)*C1 + C2, 0 == EIDeflection(1,indexB) + z(indexB)*C1 + C2 ],[C1, C2]);
    
    C1 = eval(sol1);
    C2 = eval(sol2);
    
    EITheta = -Fp*quad_sf(z,0) + Ay*quad_sf(z,a) + By*quad_sf(z,(a+b)) - (Fl)*quad_sf(z,(a+b+c))+C1;
    EIDeflection = -Fp*cubic_sf(z,0) + Ay*cubic_sf(z,a) + By*cubic_sf(z,(a+b)) - (Fl)*cubic_sf(z,(a+b+c)) + C1*z + C2;
    
    figure
    subplot(4,1,1);
    plot(z,V);
    title(['Vertical Shear Diagram of Alternating Load: ',str]);
    xlabel('Length (m)');
    ylabel('Shear (N)');
    grid on
    subplot(4,1,2);
    plot(z,M);

    title(['Vertical Moment Diagram of Alternating Load: ',str]);



    xlabel('Length (m)');
    ylabel('Moment (Nm)');
    grid on
    subplot(4,1,3);
    plot(z,EITheta);

    title(['Vertical Angular Deflection (EI*Theta) of Alternating Load: ',str]);


    xlabel('Length (m)');
    ylabel('EI*Theta (Nm^2)');
    grid on
    subplot(4,1,4);
    plot(z,EIDeflection);

    title(['Vertical Deflection (EI*Theta) Y Load, Fcf in Y',str]);

    xlabel('Length (m)');
    ylabel('EI*Deflection (Nm^3)');
    grid on

end