function [V, M, EITheta, EIDeflection,Ax,Bx] = item1HorizontalDirection(Fcf, str)
    
    % singularity functions
    step_sf = @(x,a) (x>a);
    lin_sf = @(x,a) (x-a).*(x>a);
    quad_sf = @(x,a) ((x-a).^2).*(x > a)./2;
    cubic_sf = @(x,a) ((x-a).^3).*(x > a)./6;

    %distances
    a = 0.025;
    b = 0.090;
    c = 0.200;
    g = 9.81;
    
    %dividing the linspace for 316 to get mm distances on the shaft
    z = linspace(0,a+b+c+0.00001,316);

    %Moment about A solve for Bx
    Bx = (b+c)*Fcf / b;

    Ax = -Bx + Fcf;

    disp(['When Fcf is in the horizontal direction: Ax=', num2str(Ax), 'N and Bx=', num2str(Bx), 'N.']);
    disp(' ');
    
    syms C1 C2;
    % Shear and Moment Diagrams Via Singularity Functions
    V = Ax*step_sf(z,a) + Bx*step_sf(z,(a+b)) - Fcf*step_sf(z,(a+b+c));
    M = Ax*lin_sf(z,a) + Bx*lin_sf(z,(a+b)) - Fcf*lin_sf(z,(a+b+c));
    EITheta = Ax*quad_sf(z,a) + Bx*quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c)) + C1;
    EIDeflection = Ax*cubic_sf(z,a) + Bx*cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c)) + C1*z + C2;
    
    indexA = find(abs(z-a) < 0.0001);
    indexB = find(abs(z-a-b) < 0.0001);
    
    [sol1, sol2] = solve([0 == EIDeflection(1,indexA) + z(indexA)*C1 + C2, 0 == EIDeflection(1,indexB) + z(indexB)*C1 + C2 ],[C1, C2]);
    
    C1 = eval(sol1);
    C2 = eval(sol2);
    
    EITheta = Ax*quad_sf(z,a) + Bx*quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c))+ C1;
    EIDeflection = Ax*cubic_sf(z,a) + Bx*cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c))+ C1*z + C2;
    
    figure
    subplot(4,1,1);
    plot(z,V);
    title(['Horizontal Shear Diagram of Alternating Load: ',str]);
    xlabel('Length (m)');
    ylabel('Shear (N)');
    grid on

    subplot(4,1,2);
    plot(z,M);
    title(['Horizontal Moment Diagram of Alternating Load: ',str]);
    xlabel('Length (m)');
    ylabel('Moment (Nm)');
    grid on

    subplot(4,1,3);
    plot(z,EITheta);
    title(['Horizontal Angular Deflection (EI*Theta) Load: ',str]);
    xlabel('Length (m)');
    ylabel('EI*Angle');
    grid on
    subplot(4,1,4);
    plot(z,EIDeflection);
    title(['Horizontal Deflection (EI*Y) of Alternating  Load: ',str]);
    xlabel('Length (m)');
    ylabel('EI*YDef');
    grid on
end