%         y
%         |
%         |
%         |
%          -------- z (Direction Along the Shaft)
%        /
%       /
%      /
%     x

close all;
clear all;
clc;

% const
g = 9.81;

% general
rpm_wash = 50;
rpm_dry = 1000;

m_clothes = 4.5;
m_drum = 20;
% dimensions in mm
a = 0.025;
b = 0.090;
c = 0.200;
r = 0.150;

% fatigue
life = 5500; %in hours
Sy = 490 * 10^6; %MPa
Su = 686 * 10^6; %MPa
E = 200 * 10^9; %GPa

% material
rho = 7850; %kg/m^3

%Forces
wn = pi/30*rpm_dry;

Fp = 600;
Fcf = 0.1 * m_clothes * r * wn * wn;
Fd = g * (m_clothes + m_drum);

z = linspace(0,a+b+c+0.00001,316);
M = zeros(6, length(z) );
V = zeros(6, length(z) );
Theta = zeros(6, length(z));
YDef = zeros(6, length(z));
RA = zeros(6,1);
RB = zeros(6,1);
[V(1,:),M(1,:),Theta(1,:),YDef(1,:),RA(1,1),RB(1,1)] = item1VerticalDirection(-Fcf, 'Fcf in Negative Y Direction.');
[V(2,:),M(2,:),Theta(2,:),YDef(2,:),RA(2,1),RB(2,1)] = item1VerticalDirection(Fcf, 'Fcf in Positive Y Direction.');
[V(3,:),M(3,:),Theta(3,:),YDef(3,:),RA(3,1),RB(3,1)] = item1VerticalDirection(0, 'Mean Load, Fcf in Horizontal Direction.');
[V(4,:),M(4,:),Theta(4,:),YDef(4,:),RA(4,1),RB(4,1)] = item1HorizontalDirection(Fcf, 'Fcf in Positive Y Direction');
[V(5,:),M(5,:),Theta(5,:),YDef(5,:),RA(5,1),RB(5,1)] = item1HorizontalDirection(-Fcf, 'Fcf in Positive Y Direction');
[V(6,:),M(6,:),Theta(6,:),YDef(6,:),RA(6,1),RB(6,1)] = item1HorizontalDirection(0, 'Mean Load, Fcf in Vertical Direction');

d = zeros(4,3);
% d(:,1) is Fatigue
% d(:,2) is Deflection
% d(:,3) is Vibration
[d(1,1), d(1,2), d(1,3)] = item2(M, Theta, YDef, 0    );
[d(2,1), d(2,2), d(2,3)] = item2(M, Theta, YDef, a    );
[d(3,1), d(3,2), d(3,3)] = item2(M, Theta, YDef, a+b  );
[d(4,1), d(4,2), d(4,3)] = item2(M, Theta, YDef, a+b+c);

dtemp = max(d,[],2);
dfinal = zeros(1,3);
dfinal(1,1) = max([dtemp(1) dtemp(2)]);
dfinal(1,2) = max([dtemp(2) dtemp(3)]);
dfinal(1,3) = max([dtemp(3) dtemp(4)]);

disp(['The diameter of each section: |Pulley|------', num2str(dfinal(1)), '------|A|------', num2str(dfinal(2)), '------|B|------', num2str(dfinal(3)), '------|Drum|']);
item3(RA,RB);
