clear all;
clc;
%% helper functions
step_sf = @(x,a) (x>a);
lin_sf = @(x,a) (x-a).*(x>a);
quad_sf = @(x,a) ((x-a).^2).*(x > a)./2;
cubic_sf = @(x,a) ((x-a).^3).*(x > a)./6;

%% FRAME OF REFERENCE
%         y
%         |
%         |
%         |
%          -------- z (Direction Along the Shaft)
%        /
%       /
%      /
%     x

%% design specifications
Fp = 600; % [N] - pulley force
T = 100; % [Nm] 0 - 100
n = 1000; % [rpm] 50 washing, 1000 drying 

% mass may need to change base don the wetting factor 
ml = 4.5; % [kg] dry clothes mass 
md = 20; % [kg] drum mass

% dimensions of the shaft
a = 25/1000; % [m]
b = 90/1000; % [m]
c = 200/1000; % [m]
rd = 150/1000; % [m] equivalent raidus of laundry mass
g = 9.81; % [m/s^2] 

Lh = 5500; % [hr] for 1000 rpm

% shaft material = steel alloy
sy = 490E6; % [MPa] Yeild Strength
su = 686E6; % [MPa] ultimate tensile strength
E = 200E9; % [GPa] youngs modulus 
roh = 7850; % [kg/m^3] density

%assumptions
%1. calculations use n = 1000, as that will cause the highest stress

parameters = {'Fax_Fcf_up', 'Fbx_Fcf_up', 'Fax_Fcf_down', 'Fbx_Fcf_down' ...
    'Fay_Fcf_down', 'Fby_Fcf_down', 'Fay_Fcf_up', 'Fby_Fcf_up'};
Forces_N = [];

%% resolving the forces on the shaft
Fcf = 0.1 * ml * rd * (n * (2 * pi) / 60)^2;
Fd = (md + ml)*g;
% zx plane only calc Fcf up and down because that will be the max
% force resolution in that axis/plane
% sum of moments about Max = 0 with Fcf up 
syms Fax_Fcf_up Fbx_Fcf_up;
eqn_zx_ma = b*Fbx_Fcf_up + ((b+c))*Fcf == 0;
Fbx_Fcf_up = eval(solve(eqn_zx_ma, Fbx_Fcf_up)); % Fbx 

%sum of forces in x with Fcf up
eqn_zx_fx = Fcf + Fbx_Fcf_up + Fax_Fcf_up == 0; 
Fax_Fcf_up = eval(solve(eqn_zx_fx, Fax_Fcf_up)); % Fax

Forces_N = [Forces_N; Fax_Fcf_up; Fbx_Fcf_up];

% Fcf facing downwards
% sum of momements about Ma = 0 with Fcf down
syms Fbx_Fcf_down Fax_Fcf_down;
eqn_zx_ma = b*Fbx_Fcf_down - ((b+c))*Fcf == 0;
Fbx_Fcf_down = eval(solve(eqn_zx_ma, Fbx_Fcf_down)); % Fbx 

%sum of forces in x with Fcf down
eqn_zx_fx = Fcf - Fbx_Fcf_down- Fax_Fcf_down == 0; 
Fax_Fcf_down = eval(solve(eqn_zx_fx, Fax_Fcf_down)); % Fax

Forces_N = [Forces_N; Fax_Fcf_down; Fbx_Fcf_down]

% zy plane Fcf down for max 
% sum of moments about May
syms Fay_Fcf_down Fby_Fcf_down;
eqn_zy_ma = a*Fp + b*Fby_Fcf_down - Fd*(b+c) - (b+c)*Fcf == 0;
Fby_Fcf_down = eval(solve(eqn_zy_ma, Fby_Fcf_down));

% sum of forces in y with Fcf down
eqn_zy_fy = -Fp + Fay_Fcf_down + Fby_Fcf_down - Fd - Fcf == 0;
Fay_Fcf_down = eval(solve(eqn_zy_fy, Fay_Fcf_down));

Forces_N = [Forces_N; Fay_Fcf_down; Fby_Fcf_down]

% zy plane Fcf up for min 
% sum of moments about May
syms Fay_Fcf_up Fby_Fcf_up;
eqn_zy_ma = a*Fp + b*Fby_Fcf_up - Fd*(b+c) + (b+c)*Fcf == 0;
Fby_Fcf_up = eval(solve(eqn_zy_ma, Fby_Fcf_up));

% sum of forces in y with Fcf down
eqn_zy_fy = -Fp + Fay_Fcf_up + Fby_Fcf_up - Fd + Fcf == 0;
Fay_Fcf_up = eval(solve(eqn_zy_fy, Fay_Fcf_up));

Forces_N = [Forces_N; Fay_Fcf_up; Fby_Fcf_up]

T = table(Forces_N, 'RowNames', parameters)


%% resolving shear and momement
% zx plane Fcf up
z = linspace(0, a+b+c+0.0001, 316);
v_z_up_zx_plane = Fax_Fcf_up * step_sf(z,a) + Fbx_Fcf_up * step_sf(z,(a+b)) + Fcf*step_sf(z,(a+b+c));
m_z_up_zx_plane = Fax_Fcf_up * lin_sf(z,a) + Fbx_Fcf_up * lin_sf(z,(a+b)) + Fcf*lin_sf(z,(a+b+c));

figure(1)
subplot(2,1,1);
plot(z, v_z_up_zx_plane);
title(['Horizontal Shear Diagram of Alternating X Load, Fcf in Horizontal X Up']);
xlabel('Length (m)');
ylabel('Shear (N)');
subplot(2,1,2);
plot(z, m_z_up_zx_plane);
title(['Horizontal Moment Diagram of Alternating X Load, Fcf in Horizontal X Up']);
xlabel('Length (m)');
ylabel('Moment (Nm)');

% Fcf_down
v_z_down_zx_plane = Fax_Fcf_down * step_sf(z,a) + Fbx_Fcf_down * step_sf(z,(a+b)) + Fcf*step_sf(z,(a+b+c));
m_z_down_zx_plane = Fax_Fcf_down * lin_sf(z,a) + Fbx_Fcf_down * lin_sf(z,(a+b)) + Fcf*lin_sf(z,(a+b+c));

figure(2)
subplot(2,1,1);
plot(z, v_z_down_zx_plane);
title(['Horizontal Shear Diagram of Alternating X Load, Fcf in Horizontal X Down']);
xlabel('Length (m)');
ylabel('Shear (N)');
subplot(2,1,2);
plot(z, m_z_down_zx_plane);
title(['Horizontal Moment Diagram of Alternating X Load, Fcf in Horizontal X Down']);
xlabel('Length (m)');
ylabel('Moment (Nm)');

% zy plane Fcf up 
v_z_up_zy_plane = -Fp * step_sf(z,0) + Fay_Fcf_up * step_sf(z,a) + Fby_Fcf_up * step_sf(z,(a+b)) + Fcf*step_sf(z,(a+b+c)) - Fd * step_sf(z,(a+b+c));
m_z_up_zy_plane = -Fp * lin_sf(z,0) + Fay_Fcf_up * lin_sf(z,a) + Fby_Fcf_up * lin_sf(z,(a+b)) + Fcf*lin_sf(z,(a+b+c)) - Fd * lin_sf(z,(a+b+c));

figure(3)
subplot(2,1,1);
plot(z, v_z_up_zy_plane);
title(['Vertical Shear Diagram of Alternating X Load, Fcf in Horizontal X Up']);
xlabel('Length (m)');
ylabel('Shear (N)');
subplot(2,1,2);
plot(z, m_z_up_zy_plane);
title(['Vertical Moment Diagram of Alternating X Load, Fcf in Horizontal X Up']);
xlabel('Length (m)');
ylabel('Moment (Nm)');

v_z_down_zy_plane = -Fp * step_sf(z,0) + Fay_Fcf_down * step_sf(z,a) + Fby_Fcf_down * step_sf(z,(a+b)) - Fcf*step_sf(z,(a+b+c)) - Fd * step_sf(z,(a+b+c));
m_z_down_zy_plane = -Fp * lin_sf(z,0) + Fay_Fcf_down * lin_sf(z,a) + Fby_Fcf_down * lin_sf(z,(a+b)) - Fcf*lin_sf(z,(a+b+c)) - Fd * lin_sf(z,(a+b+c));

figure(4)
subplot(2,1,1);
plot(z, v_z_down_zy_plane);
title(['Vertical Shear Diagram of Alternating X Load, Fcf in Horizontal X Down']);
xlabel('Length (m)');
ylabel('Shear (N)');
subplot(2,1,2);
plot(z, m_z_down_zy_plane);
title(['Vertical Moment Diagram of Alternating X Load, Fcf in Horizontal X Down']);
xlabel('Length (m)');
ylabel('Moment (Nm)');

%% resolving deflections
syms C3 C4
%zx plane Fcf up
% index= find(abs(z-location) < 0.0001);
indexA = find(abs(z-a) < 0.0001);
indexB = find(abs(z-(a+b)) < 0.0001);
EI_deflection_angle_zx_plane = Fax_Fcf_up * quad_sf(z,a) + Fbx_Fcf_up * quad_sf(z,(a+b)) + Fcf*quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zx_plane = Fax_Fcf_up * cubic_sf(z,a) + Fbx_Fcf_up * cubic_sf(z,(a+b)) + Fcf*cubic_sf(z,(a+b+c)) + C3*z + C4;

eqn1 = EI_deflection_dist_zx_plane(indexA) == 0;
eqn2 = EI_deflection_dist_zx_plane(indexB) == 0;

sol = solve ([eqn1, eqn2], [C3, C4]);
C3 = eval(sol.C3);
C4 = eval(sol.C4);

EI_deflection_angle_zx_plane = Fax_Fcf_up * quad_sf(z,a) + Fbx_Fcf_up * quad_sf(z,(a+b)) + Fcf*quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zx_plane = Fax_Fcf_up * cubic_sf(z,a) + Fbx_Fcf_up * cubic_sf(z,(a+b)) + Fcf*cubic_sf(z,(a+b+c)) + C3*z + C4;

figure(5)
subplot(2,1,1);
plot(z, EI_deflection_angle_zx_plane);
title(['Horizontal Angular Deflection (EI*Theta) Y Load, Fcf Up']);
xlabel('Length (m)');
ylabel('EI*Angle');
subplot(2,1,2);
plot(z, EI_deflection_dist_zx_plane);
title(['Horizontal Deflection (EI*Theta) Y Load, Fcf in Up']);
xlabel('Length (m)');
ylabel('EI*YDef');

% zx plane Fcf down 
syms C3 C4
EI_deflection_angle_zx_plane = Fax_Fcf_up * quad_sf(z,a) + Fbx_Fcf_up * quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zx_plane = Fax_Fcf_up * cubic_sf(z,a) + Fbx_Fcf_up * cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c)) + C3*z + C4;

eqn1 = EI_deflection_dist_zx_plane(indexA) == 0;
eqn2 = EI_deflection_dist_zx_plane(indexB) == 0;

sol = solve ([eqn1, eqn2], [C3, C4]);
C3 = eval(sol.C3);
C4 = eval(sol.C4);

EI_deflection_angle_zx_plane = Fax_Fcf_up * quad_sf(z,a) + Fbx_Fcf_up * quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zx_plane = Fax_Fcf_up * cubic_sf(z,a) + Fbx_Fcf_up * cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c)) + C3*z + C4;

figure(6)
subplot(2,1,1);
plot(z, EI_deflection_angle_zx_plane);
title(['Horizontal Angular Deflection (EI*Theta) Y Load, Fcf Down']);
xlabel('Length (m)');
ylabel('EI*Angle');
subplot(2,1,2);
plot(z, EI_deflection_dist_zx_plane);
title(['Horizontal Deflection (EI*Theta) Y Load, Fcf in Down']);
xlabel('Length (m)');
ylabel('EI*YDef');

%zy plane Fcf up
syms C3 C4
EI_deflection_angle_zy_plane_up = -Fp * quad_sf(z,0) + Fay_Fcf_up * quad_sf(z,a) + Fby_Fcf_up * quad_sf(z,(a+b)) + Fcf*quad_sf(z,(a+b+c)) - Fd * quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zy_plane_up = -Fp * cubic_sf(z,0) + Fay_Fcf_up * cubic_sf(z,a) + Fby_Fcf_up * cubic_sf(z,(a+b)) + Fcf*cubic_sf(z,(a+b+c)) - Fd * cubic_sf(z,(a+b+c)) + C3*z + C4;

eqn1 = EI_deflection_dist_zy_plane_up(indexA) == 0;
eqn2 = EI_deflection_dist_zy_plane_up(indexB) == 0;

sol = solve ([eqn1, eqn2], [C3, C4]);
C3 = eval(sol.C3);
C4 = eval(sol.C4);

EI_deflection_angle_zy_plane_up = -Fp * quad_sf(z,0) + Fay_Fcf_up * quad_sf(z,a) + Fby_Fcf_up * quad_sf(z,(a+b)) + Fcf*quad_sf(z,(a+b+c)) - Fd * quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zy_plane_up = -Fp * cubic_sf(z,0) + Fay_Fcf_up * cubic_sf(z,a) + Fby_Fcf_up * cubic_sf(z,(a+b)) + Fcf*cubic_sf(z,(a+b+c)) - Fd * cubic_sf(z,(a+b+c))+ C3*z + C4;

figure(7)
subplot(2,1,1);
plot(z, EI_deflection_angle_zy_plane_up);
title(['Vertical Angular Deflection (EI*Theta) Y Load, Fcf Up']);
xlabel('Length (m)');
ylabel('EI*Angle');
subplot(2,1,2);
plot(z, EI_deflection_dist_zy_plane_up);
title(['Vertical Deflection (EI*Theta) Y Load, Fcf in Up']);
xlabel('Length (m)');
ylabel('EI*YDef');

% zy plane Fcf Down
syms C3 C4
EI_deflection_angle_zy_plane = -Fp * quad_sf(z,0) + Fay_Fcf_down * quad_sf(z,a) + Fby_Fcf_down * quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c)) - Fd * quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zy_plane = -Fp * cubic_sf(z,0) + Fay_Fcf_down * cubic_sf(z,a) + Fby_Fcf_down * cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c)) - Fd * cubic_sf(z,(a+b+c)) + C3*z + C4;

eqn1 = EI_deflection_dist_zy_plane(indexA) == 0;
eqn2 = EI_deflection_dist_zy_plane(indexB) == 0;

sol = solve([eqn1, eqn2], [C3, C4]);
C3 = eval(sol.C3);
C4 = eval(sol.C4);

EI_deflection_angle_zy_plane = -Fp * quad_sf(z,0) + Fay_Fcf_down * quad_sf(z,a) + Fby_Fcf_down * quad_sf(z,(a+b)) - Fcf*quad_sf(z,(a+b+c)) - Fd * quad_sf(z,(a+b+c)) + C3;
EI_deflection_dist_zy_plane = -Fp * cubic_sf(z,0) + Fay_Fcf_down * cubic_sf(z,a) + Fby_Fcf_down * cubic_sf(z,(a+b)) - Fcf*cubic_sf(z,(a+b+c)) - Fd * cubic_sf(z,(a+b+c)) + C3*z + C4;

figure(8)
subplot(2,1,1);
plot(z, EI_deflection_angle_zy_plane);
title(['Vertical Angular Deflection (EI*Theta) Y Load, Fcf Down']);
xlabel('Length (m)');
ylabel('EI*Angle');
subplot(2,1,2);
plot(z, EI_deflection_dist_zy_plane);
title(['Vertical Deflection (EI*Theta) Y Load, Fcf in Down']);
xlabel('Length (m)');
ylabel('EI*YDef');

