function [d_fatigue, d_deflection, d_resonance] = item2(Moment, Theta, YDef, location)
    %% fatigue design
    life = 5500; %in hours
    Sy = 490 * 10^6; %MPa
    Su = 686 * 10^6; %Mpa
    E = 200 * 10^9; %Mpa
    g = 9.81;
    a = 0.025;
    b = 0.090;
    c = 0.200;
    z = linspace(0,a+b+c+0.00001,316);
    
    index = find(abs(z-location) < 0.0001);
    Mmin = Moment(1,index);
    Mmax = Moment(2,index) ;
    
    %get C factors using table in Tutorial
    Cm = 0.8; %for cast steel
    Cs = 0.86; %assume diameter = 0.03m
    Cst = 1;
    Cr = 0.814;
    Nf = 2;
    
    Tmax = 100;
    Tmin = 0;
    
    Ma = (Mmax-Mmin)/2;
    Mm = (Mmax+Mmin)/2;
    Ta = (Tmax-Tmin)/2;
    Tm = (Tmax+Tmin)/2;

    disp('doneeeeee');
    rfillet = 0.0011; %selected from catalogue
    
    if (location < b+a)
        %Use Appendix E-2
        %D/d = 1.36 ~= 1.2 (estimate)
        A = 0.97098;
        B = -0.21798;
        Kt = A*(rfillet/0.022)^B;
        %Kt = 2.030
        
        %Use Appendix E-3
        %D/d ~= 1.36 = 1.33 (closest estimate)
        A = 0.84897;
        B = -0.23161;
        Kts = A*(rfillet/0.022)^B;
        %Kts = 1.699;
        
        Cs = 0.881;
    else
        %Use Appendix E-2
        %D/d ~= 1 = 1.02 (estimate)
        A = 0.96408;
        B = -0.11711;
        Kt = A*(rfillet/0.03)^B;
        %Kt = 1.996;
        
        %Use Appendix E-3
        %D/d ~= 1 = 1.09 (closest estimate)
        A = 0.90337;
        B = -0.12692;
        Kts = A*(rfillet/0.03)^B;
        %Kts = 1.707;
        
        Cs = 0.855;
    end
    q = 0.75; %pages stress concentration factors Figure 6-20
    qshear = 0.8; %pages stress concentration factors Figure 6-21
    Kf  = 1 + q*(Kt-1)
    Kfs = 1 +qshear*(Kts-1)
    Kfm = Kf; %assume less than max stress is less than Sy/Kf, this means Kfm == Kf

    Sut = Su;
    
    Sn = 0.5*Sut;
    Snprime = Cm*Cst*Cs*Cr*Sn;
    d_fatigue = ( 32*Nf/pi * (sqrt((Kf*Ma)^2 + 3/4*(Kfs*Ta)^2)/Snprime + sqrt((Kfm*Mm)^2 + 3/4*(Kfm*Tm)^2)/Sut) )^(1/3);
    
    %% Deflection
    EITheta = abs(Theta(1,index));
    ThetaMax = 0.003;
    %Re-arranged equation from notes
    d_deflection = (64*EITheta/(pi*E*ThetaMax))^(1/4);
    
    if (location ~= a && location ~= a+b)
        d_deflection = 0;
    end
    
    %% Vibration
    rho = 7850; %kg/m^3
    rpm_dry = 1000;
    w = pi/30*rpm_dry;
    
    [~, index] = max(abs(YDef(1,:)));
    Ymax = YDef(1,index);
    
    
    m_clothes = 4.5;
    m_drum = 20;
    r = 0.150;
    w = pi/30*rpm_dry;
    Fcf = 0.1 * m_clothes * r * w * w;
    Fd = g * (m_clothes + m_drum);
    
    Fl = Fcf + Fd;
    
    if (location < a)
       %Use technique for cantilever with mass
       syms d;
       l = a;
       Fp = 600;
       delta = (rho*l^4*g)/(8*E*d^2/16) + Fp*l^3/(3*E*pi*d^4/64);
       eqn = 1000 == 0.75*30/pi*sqrt(g/delta);
       
       d_resonance = abs(vpasolve(eqn,d));
       
    elseif (location >= a && location <= a+b)
       %Use technique for two supports with distributed load
       l = b;
       d_resonance = (rpm_dry*pi*l^2*sqrt(5*rho))/(0.75*30*sqrt(24*E));
       
    elseif( location > a+b)
       %Use technique for cantilever with mass
       syms d;
       l = c;
       
       delta = (rho*l^4*g)/(8*E*d^2/16) + Fl*l^3/(3*E*pi*d^4/64);
       eqn = 1000 == 0.75*30/pi*sqrt(g/delta);
       
       d_resonance = abs(vpasolve(eqn,d));
    end
    
end
